import React from 'react'
import PropTypes from 'prop-types'
import solarRooftopText from '../assets/solar-rooftop-text.png'
import logo from '../assets/logo.png'

const Header = props => {
  return (
    <div style={{
      height: 215,
      paddingLeft: 30,
      paddingRight: 30,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }}>
      <img src={logo} alt="logo" width={'60%'} />
      <img src={solarRooftopText} alt="text" width={'25%'} />
    </div>
  )
}

Header.propTypes = {}

export default Header
