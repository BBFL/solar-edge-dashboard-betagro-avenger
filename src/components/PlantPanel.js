import React from 'react'
import PropTypes from 'prop-types'
import Odometer from './Odometer'
import plant from '../assets/plant.png'
import solarPanels from '../assets/solar-panels.png'
import inverter from '../assets/inverter.png'
import loadPanels from '../assets/load-panels.png'

const StatusPanel = props => {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        marginLeft: 70,
        zIndex: 1000,
        width: '40%',
        paddingTop: 15,
        ...props.style
      }}
    >
      <img src={props.img} alt={props.alt} width={115} height={115} />
      <div style={{ padding: 15 }}>
        <h3 style={{ color: '#110842', margin: 0 }}>{props.title}</h3>
        <p style={{ marginTop: 5 }}>{props.description}</p>
        {props.hasValue && <Odometer digits={5} value={props.value} />}
      </div>
    </div>
  )
}

const PlantPanel = props => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', position: 'relative' }}>
      <img src={plant} alt='plant' width={'80%'} style={{ position: 'absolute', top: 15, left: 120 }} />
      <StatusPanel style={{ paddingTop: 90 }} img={solarPanels} alt={'solar-panels'} title={'Solar Panels'}
                   description={'Convert sunlight into DC electricity'} hasValue value={0} />
      <StatusPanel img={inverter} alt={'inverter'} title={'Inverter'}
                   description={'Convert DC electricity into AC electricity'} />
      <StatusPanel img={loadPanels} alt={'inverter'} title={'Load Panels'}
                   description={'Receives the AC electricity to be used to power to factory'} hasValue value={0} />
    </div>
  )
}

PlantPanel.propTypes = {}

export default PlantPanel
