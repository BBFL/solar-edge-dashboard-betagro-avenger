import React from 'react'
import PropTypes from 'prop-types'
import Odometer from './Odometer'
import moneyEarn from '../assets/moneyEarn.png'
import energySaved from '../assets/energySaved.png'
import coEmissions from '../assets/coEmissions.png'
import operationDay from '../assets/operationDay.png'

const monitorType = {
  moneyEarn: {
    description: 'Total money earn (Baht)',
    imgPath: moneyEarn,
    digits: 10
  },
  energySaved: {
    description: 'Total save energy (kWh)',
    imgPath: energySaved,
    digits: 10
  },
  coEmissions: {
    description: 'Total CO Emissions Avoided (Tons)',
    imgPath: coEmissions,
    digits: 10
  },
  operationDay: {
    description: 'Total operation day from COD (Day)',
    imgPath: operationDay,
    digits: 5
  }
}

const MonitorPanel = props => {
  const { type = 'moneyEarn', value = 0 } = props
  const data = monitorType[type]
  return (
    <div style={{
      display: 'flex',
      background: 'white',
      width: '45%',
      borderRadius: 25
    }}>
      <img src={data.imgPath} height={130} alt={'panel'} style={{
        marginLeft: -40
      }}/>
      <div style={{
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}>
        <Odometer digits={data.digits} value={value} />
        <p style={{ color: 'black', margin: 0, marginTop: 15 }}>
          {data.description}
        </p>
      </div>
    </div>
  )
}

MonitorPanel.propTypes = {}

export default MonitorPanel
