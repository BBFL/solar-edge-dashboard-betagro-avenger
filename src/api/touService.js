import axios from 'axios'
import config from '../config'

const host = 'https://us-central1-nortis-5ca06.cloudfunctions.net'

export const getPrice = date => axios.post(
  `${host}/getPrice`,
  {
    'current_date': date
  }).then(({ data }) => data)
  .catch((error) => {
    throw  error
  })

export const getTotalPrice = date => axios.post(
  `${host}/getTotalPrice`,
  {
    'begin_date': config.startDate,
    'current_date': date
  }).then(({ data }) => data)
  .catch((error) => {
    throw  error
  })
