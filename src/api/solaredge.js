import jsonp from 'jsonp'
import config from '../config'

const host = `https://monitoringapi.solaredge.com/site/${config.siteId}`


export const getPower = date => {
  return new Promise((resolve, reject) => {
    jsonp(
      `${host}/powerDetails?startTime=${date} 00:00:00&endTime=${date} 23:30:00&api_key=${config.apiKey}`,
      null,
      (err, data) => {
        if (err) {
          return reject(err)
        } else {
          return resolve(data)
        }
      }
    )
  })
}

export const getEnergy = (startDate, endDate, timeUnit) => {
  return new Promise((resolve, reject) => {
    jsonp(
      `${host}/energy?startDate=${startDate}&endDate=${endDate}&timeUnit=${timeUnit}&api_key=${config.apiKey}`,
      null,
      (err, data) => {
        if (err) {
          return reject(err)
        } else {
          return resolve(data)
        }
      }
    )
  })
}
