import { action, observable } from 'mobx'
import moment from './App'
import { getEnergy } from './api/solaredge'

class AppState {
  @observable currentTime = moment()
  @observable totalEnergy = 0

  constructor() {
    setInterval(() => {
      this.currentTime = moment()
    }, 1000);
  }

  @action
  getData() {
    getEnergy('2018-12-08', '2019-02-18', 'YEAR').then(({energy}) => {
      console.log(energy.values.reduce((data, sum) => sum.value + data.value))
      this.totalEnergy = energy.values.reduce((data, sum) => sum + data.value)
    })
  }
}
const appState = new AppState()
export default appState